package com.clicklabs.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import com.clicklabs.assignment.entities.*;
import com.clicklabs.assignment.util.AppConst;

public class Admin implements AppConst {
	public static Scanner scan = new Scanner(System.in);
	public static Map<Integer, Customer> customerIdMap = new HashMap<Integer, Customer>();

	public static void main(String[] args) {
		do {
			System.out.println("1. Add Customer");
			System.out.println("2. Add Car to Customer");
			System.out.println("3. Display customer data(order by car model)");
			System.out.println("4. Display data of specific customer");
			System.out.println("5. Prize");
			System.out.println("6. Exit");
			System.out.print("Option:");

			if (scan.hasNextInt()) {
				int option = scan.nextInt();
				switch (option) {
				case ADD_CUSTOMER:
					addCustomer();
					break;
				case ADD_CAR:
					addCar();
					break;
				case DISPLAY_ALL_CUSTOMER:
					displayAll();
					break;
				case DISPLAY_CUSTOMER:
					displaySpecificCustomer();
					break;
				case PRIZE:
					prize();
					break;
				case EXIT:
					System.out.println("Thanks for using Car Sales.");
					break;
				default:
					System.err
							.println("Invalid selection....Please Select Valid Option.");
					System.out.println();
				}
			} else {
				System.out
						.println("The option entered is not valid,Please try again..!!");
				System.out.println();

			}
		} while (true);
	}

	private static void prize() {
		Set<Integer> set = customerIdMap.keySet();
		List<Integer> keyList = new ArrayList<Integer>(set);
		Random rn = new Random();
		Set<Integer> pId = new HashSet<Integer>();
		for (int i = 0; i <= 5; i++) {

			int pSize = rn.nextInt(keyList.size());
			int prize = keyList.get(pSize);

			pId.add(prize);
		}

		Set<Integer> prId = new HashSet<Integer>();
		System.out.println("enter 3 customer ids");
		for (int i = 0; i <= 2; i++) {
			prId.add(scan.nextInt());

		}

		Set<Integer> prizeId = new HashSet<Integer>();

		for (int a : pId) {
			for (int b : prId) {
				if (a == b) {
					prizeId.add(a);

				}
			}
		}

		System.out.println("prizes received by " + prizeId);
	}

	public static void addCustomer() {

		System.out.print("Enter Customer id:");
		if (scan.hasNextInt()) {
			int id = scan.nextInt();

			if (customerIdMap.containsKey(id)) {

				System.out
						.println("Customer already exists,please choose other id..!!");
				System.out.println();
				addCustomer();
			} else {

				System.out.print("Enter Customer name:");
				String name = scan.next();
				customerIdMap.put(id, new Customer(id, name));
				System.out.println();
				System.out.println("Customer Added Sucessfully");
				System.out.println();
			}

		} else {
			System.out.println("ID Already exist. Please enter another ID");
			System.out.println();
			addCustomer();
		}
	}

	public static void addCar() {

		System.out.print("Enter id of customer who wants to buy car:");
		int idOfCustomer = scan.nextInt();

		if (customerIdMap.containsKey(idOfCustomer)) {
			System.out.print("Enter Brand name:");
			String brand = scan.next().toLowerCase();
			System.out.print("Enter Model:");
			String carModel = scan.next();
			System.out.print("Enter Car id:");
			int carID = scan.nextInt();
			System.out.print("Enter Price:");
			int carPrice = scan.nextInt();
			switch (brand) {
			case MARUTI:
				Maruti maruti = new Maruti(carID, carPrice, carModel);
				customerIdMap.get(idOfCustomer).getCar().add(maruti);
				System.out.println("Car " + carModel.toUpperCase()
						+ " Sucessfully added in "
						+ customerIdMap.get(idOfCustomer).customer_name
						+ "'s list of cars.");
				System.out.println();
				break;
			case HYUNDAI:
				Hyundai hyundai = new Hyundai(carID, carPrice, carModel);
				customerIdMap.get(idOfCustomer).getCar().add(hyundai);
				System.out.println("Car " + carModel.toUpperCase()
						+ " Sucessfully added in "
						+ customerIdMap.get(idOfCustomer).customer_name
						+ "'s list of cars.");
				System.out.println();
				break;
			case TOYOTA:
				Toyota toyota = new Toyota(carID, carPrice, carModel);
				customerIdMap.get(idOfCustomer).getCar().add(toyota);
				System.out.println("Car " + carModel.toUpperCase()
						+ " Sucessfully added in "
						+ customerIdMap.get(idOfCustomer).customer_name
						+ "'s list of cars.");
				System.out.println();

			default:
				System.out.println("Brand is not recognized");
				addCar();

			}
		} else {
			System.out
					.println("Customer does not exist,Enter details for new customer");
			System.out.println();
			addCustomer();

		}

	}

	public static void displayAll() {
		List<Customer> customerList = new ArrayList<Customer>();
		for (int i = 0; i < customerList.size(); i++) {
			customerList.get(i).display();
		}

	}

	public static void displaySpecificCustomer() {
		System.out.print("Enter id of customer:");
		if (scan.hasNextInt()) {
			int id = scan.nextInt();
			if (customerIdMap.containsKey(id)) {
				Customer customer = customerIdMap.get(id);
				customer.display();
			} else {
				System.out.println();
				System.out.println("This id does not exist,Please try again");
				System.out.println();
				displaySpecificCustomer();
			}
		} else {
			System.out.println();
			System.out.println("Wrong id type,Please try again");
			System.out.println();
			displaySpecificCustomer();
		}
	}

}
