package com.clicklabs.assignment.entities;

import java.util.ArrayList;
import java.util.List;

public class Customer {
	public int customer_id;
	public String customer_name;
	public List<Cars> cars = new ArrayList<Cars>();
		public List<Cars> getCar() {
		return cars;
	}

	public Customer(int id, String name) {
		this.customer_id = id;
		this.customer_name = name;
	}
	public String getName() {
		return customer_name;
	}
	public Customer() {
		// TODO Auto-generated constructor stub
	}

	public void display() {
		System.out.println();
		System.out.println("Customer id :" + customer_id);
		System.out.println("Customer name :" + customer_name);
		System.out.println();
		System.out.println("Cars owned by customer name:" + customer_name);
		System.out.println("*************************************");
		if (cars.size() > 0) {
			for (int i = 0; i < cars.size(); i++) {

				System.out.println("Car model:" + cars.get(i).model);
				System.out.println("Car Id :" + cars.get(i).id);
				System.out.println("Car Brand " + cars.get(i).brand);
				System.out.println("Car Price:" + cars.get(i).price);
				System.out.println("Car Resale Value :"
						+ cars.get(i).resaleValue);
				System.out.println();
				System.out
						.println("------------------------------------------");
			}
			System.out.println("*************************************");
		} else {
			System.out.println("No Cars owned by customer " + customer_id);
			System.out.println();
			System.out.println("------------------------------------------");
		}
	}

	
}
