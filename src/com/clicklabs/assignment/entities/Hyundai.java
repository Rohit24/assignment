package com.clicklabs.assignment.entities;

public class Hyundai extends Cars
{

	public Hyundai(int id, int price, String model) 
	{
		this.id=id;
		this.price=price;
		this.model=model;
		this.resaleValue=(price*4)/10;
		brand="Hyundai";

	}

}
