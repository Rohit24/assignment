package com.clicklabs.assignment.entities;

public class Toyota extends Cars
{


	public Toyota(int id, int price, String model) 
	{
		this.id=id;
		this.price=price;
		this.model=model;
		this.resaleValue=(price*8)/10;
		brand="Toyota";
	}


}
