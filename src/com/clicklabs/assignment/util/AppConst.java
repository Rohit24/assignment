package com.clicklabs.assignment.util;

public interface AppConst{
	int ADD_CUSTOMER = 1;
	int ADD_CAR = 2;
	int DISPLAY_ALL_CUSTOMER = 3;
	int DISPLAY_CUSTOMER = 4;
	int PRIZE = 5;
	int EXIT = 6;
	String HYUNDAI = "hyundai";
	String TOYOTA = "toyota";
	String MARUTI = "maruti";
}
